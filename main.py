
from functions import *
import os 

ip_nifi='192.168.1.11'
ip_kafka='192.168.3.2' 
ip_bastion='35.211.81.21'
username='arnoldvelasquez'
port='22'
filename='/home/arnoldvelasquez/' + 'arnoldvelasquez'
filename_mac='/Volumes/External_Arnold/IPT/ssh-bastion/' + 'arnoldvelasquez'
local_directory=os.getcwd()

for vm in ['nifi','kafka']:

    if vm=='nifi':
        ip=ip_nifi
    else:
        ip=ip_kafka
        
    channel,sshcon=login_ssh_vm(ip,username,filename,port)
    channel.send('df -h ; top -b -n 1 '+'\n')
    time.sleep(1)
    output = channel.recv(65535).decode("utf-8")
    list = output.splitlines()
    output = [line.rstrip() for line in list] 
    #print (''.join(output))
    #print(output)
    file = open('output_%s.txt' %vm, 'w')
    file.write('\n'.join(output))
    file.close()
    logout_ssh_(sshcon)


send_mail_with_attach([local_directory+'/' + 'output_nifi.txt', local_directory+'/' + 'output_kafka.txt'])
