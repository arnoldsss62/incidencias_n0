Last login: Thu Oct 29 14:51:03 2020 from 179.7.136.12

[arnoldvelasquez@gce-ipt-bastion-production ~]$ df -h;top -b -n 1
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        912M     0  912M   0% /dev
tmpfs           919M     0  919M   0% /dev/shm
tmpfs           919M   97M  822M  11% /run
tmpfs           919M     0  919M   0% /sys/fs/cgroup
/dev/sda2       9.8G  5.9G  4.0G  60% /
/dev/sda1       200M   12M  189M   6% /boot/efi
tmpfs           184M     0  184M   0% /run/user/1018
tmpfs           184M     0  184M   0% /run/user/1035
tmpfs           184M     0  184M   0% /run/user/1042
tmpfs           184M     0  184M   0% /run/user/1036
tmpfs           184M     0  184M   0% /run/user/1011
tmpfs           184M     0  184M   0% /run/user/1014
tmpfs           184M     0  184M   0% /run/user/1034
top - 14:53:49 up 253 days, 18:45,  6 users,  load average: 0.00, 0.01, 0.05
Tasks: 105 total,   2 running, 103 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us, 12.5 sy,  0.0 ni, 87.5 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  1881292 total,   294732 free,   252316 used,  1334244 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  1342352 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
    1 root      20   0  128128   5940   3344 S  0.0  0.3   9:39.08 systemd
    2 root      20   0       0      0      0 S  0.0  0.0   0:07.12 kthreadd
    4 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kworker/0:0H
    6 root      20   0       0      0      0 S  0.0  0.0   1:53.13 ksoftirqd/0
    7 root      rt   0       0      0      0 S  0.0  0.0   0:00.00 migration/0
    8 root      20   0       0      0      0 S  0.0  0.0   0:00.00 rcu_bh
    9 root      20   0       0      0      0 R  0.0  0.0   2:30.45 rcu_sched
   10 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 lru-add-dra+
   11 root      rt   0       0      0      0 S  0.0  0.0   2:02.18 watchdog/0
   13 root      20   0       0      0      0 S  0.0  0.0   0:00.00 kdevtmpfs
   14 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 netns
   15 root      20   0       0      0      0 S  0.0  0.0   0:07.32 khungtaskd
   16 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 writeback
   17 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kintegrityd
   18 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 bioset
   19 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 bioset
   20 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 bioset
   21 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kblockd
   22 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 md
   23 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 edac-poller
   24 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 watchdogd
   30 root      20   0       0      0      0 S  0.0  0.0   1:23.42 kswapd0
   31 root      25   5       0      0      0 S  0.0  0.0   0:00.00 ksmd
   32 root      39  19       0      0      0 S  0.0  0.0   1:00.60 khugepaged
   33 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 crypto
   41 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kthrotld
   43 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kmpath_rdacd
   44 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kaluad
   45 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kpsmoused
   47 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 ipv6_addrco+
   60 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 deferwq
   99 root      20   0       0      0      0 S  0.0  0.0   0:10.80 kauditd
  178 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 virtscsi-sc+
  179 root      20   0       0      0      0 S  0.0  0.0   0:00.00 scsi_eh_0
  180 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 scsi_tmf_0
  193 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 bioset
  194 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfsalloc
  195 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs_mru_cac+
  196 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-buf/sda2
  197 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-data/sd+
  198 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-conv/sd+
  199 root       0 -20       0      0      0 S  0.0  0.0   0:00.05 xfs-cil/sda2
  200 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-reclaim+
  201 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-log/sda2
  202 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 xfs-eofbloc+
  203 root      20   0       0      0      0 S  0.0  0.0  35:39.95 xfsaild/sda2
  204 root       0 -20       0      0      0 S  0.0  0.0   0:55.03 kworker/0:1H
  266 root      20   0   39084   7648   7332 S  0.0  0.4  10:45.54 systemd-jou+
  343 root      20   0       0      0      0 S  0.0  0.0   0:03.28 hwrng
  404 root      16  -4   55528    884    448 S  0.0  0.0   1:43.96 auditd
  433 dbus      20   0   66584   1796    972 S  0.0  0.1   2:19.48 dbus-daemon
  437 root      20   0    4388    436    296 S  0.0  0.0   0:00.00 acpid
  455 chrony    20   0   96268   1020    712 S  0.0  0.1   0:16.06 chronyd
  489 root      20   0  110108    444    316 S  0.0  0.0   0:00.00 agetty
  490 root      20   0  110108    452    324 S  0.0  0.0   0:00.00 agetty
  520 root      20   0  547936   5304   3168 S  0.0  0.3   9:51.80 NetworkMana+
  674 root      20   0  102896   4132   2008 S  0.0  0.2   0:37.16 dhclient
 1859 root      20   0       0      0      0 S  0.0  0.0   0:01.31 kworker/u2:1
 6590 root      20   0  119440  14300   8196 S  0.0  0.8  23:55.16 google_osco+
 6855 root      20   0  158928   5692   4356 S  0.0  0.3   0:00.22 sshd
 6858 edgarfu+  20   0  159060   2528   1084 S  0.0  0.1   0:07.69 sshd
 8736 root      20   0  158928   5684   4356 S  0.0  0.3   0:00.21 sshd
 8739 edgarfu+  20   0  158928   2300    972 S  0.0  0.1   0:00.13 sshd
 8858 root      20   0  158928   5684   4344 S  0.0  0.3   0:00.31 sshd
 8861 jeanram+  20   0  158928   2460   1112 S  0.0  0.1   0:00.52 sshd
 8862 jeanram+  20   0  115548   2012   1612 S  0.0  0.1   0:00.01 bash
10814 root      20   0  308664   9296   7652 S  0.0  0.5  14:30.78 rsyslogd
22360 root      20   0  114268  10196   3940 S  0.0  0.5   7:03.47 google_gues+
22432 root      20   0  112924   4320   3296 S  0.0  0.2   1:01.14 sshd
22441 root      20   0   26624   1900   1352 S  0.0  0.1   0:36.41 systemd-log+
22451 root      20   0  126388   1660   1032 S  0.0  0.1   0:18.06 crond
23277 root      20   0  158928   5684   4356 S  0.0  0.3   0:00.20 sshd
23280 edgarfu+  20   0  158928   2300    972 S  0.0  0.1   0:00.08 sshd
24129 root      20   0   47480   3496   1924 S  0.0  0.2   0:00.06 systemd-ude+
26477 root      20   0       0      0      0 S  0.0  0.0   0:01.11 kworker/u2:2
28263 root      20   0  158928   5572   4228 S  0.0  0.3   0:00.20 sshd
28266 carlosr+  20   0  160224   3740   1208 S  0.0  0.2   0:05.59 sshd
28267 carlosr+  20   0  115548   2072   1660 S  0.0  0.1   0:00.01 bash
28817 root      20   0  158924   5648   4312 S  0.0  0.3   0:00.19 sshd
28820 vanessa+  20   0  159060   2540   1100 S  0.0  0.1   0:02.93 sshd
28934 root      20   0  158928   5708   4372 S  0.0  0.3   0:00.20 sshd
28937 luisgar+  20   0  159064   2452   1104 S  0.0  0.1   0:00.41 sshd
28938 luisgar+  20   0  115548   2020   1612 S  0.0  0.1   0:00.01 bash
29012 root      20   0  158928   5628   4296 S  0.0  0.3   0:00.19 sshd
29017 luisgar+  20   0  160988   4588   1100 S  0.0  0.2   0:06.01 sshd
29060 root      20   0  358852  27424   4984 S  0.0  1.5   0:18.23 firewalld