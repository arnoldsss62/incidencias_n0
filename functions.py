
import paramiko
import os
import sys
import subprocess
import traceback
import time
from datetime import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.message import EmailMessage
from email.mime.base import MIMEBase
from email import encoders

def login_ssh_vm(ip_vm, username, fileName, port):
    print("Welcome Bastion, Enjoy your journey!")
    sshcon = paramiko.SSHClient()
    sshcon.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        sshcon.connect(ip_vm, username = username, key_filename=fileName, port=port)
    except:
        print("errores conexion ssh")
        traceback.print_exc()
        sshcon.close()
        return 0
    else:
       #print("Connection Open")
        channel = sshcon.invoke_shell(term='vt100', width=80, height=24, width_pixels=0, height_pixels=0, environment=None)
    return channel, sshcon
    
def logout_ssh_(sshcon):
    sshcon.close()
    print("Logout , Thanks!")
    


def send_mail_with_attach(attachment_path_list):
    
    print("hola entraste a la funcion de enviar correo")
    recipients = ['stewart.garzon@ipt.pe','arnold.velasquez@ipt.pe','arnoldsss62@gmail.com']
    
    body_content='Esta es una prueba de envio de status de VM de Nifi y kafka  y bastion'
    
    message = MIMEMultipart()
    message['Subject'] = 'Daily Health Check Nifi/Kafka'
    message['From'] = 'software_factory@ipt.pe'
    message['To'] = ', '.join(recipients)
    
    
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            
            print(each_file_path)
            
            file_name=each_file_path.split("/")[-1]
            print(file_name)
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(each_file_path, "rb").read())

            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
            message.attach(part)
            
            #print ("could not attache file")
                
    message.attach(MIMEText(body_content))
    
    '''
    body_content = body
    message.attach(MIMEText(body_content, "html"))
    '''
    msg_body = message.as_string()

    
    
    mailserver = smtplib.SMTP('smtp.office365.com',587)
    mailserver.ehlo()
    mailserver.starttls()
    mailserver.login('software_factory@ipt.pe', 'IPT2020#')
    #mailserver.send_message(msg_body)
    mailserver.sendmail('software_factory@ipt.pe', recipients , msg_body)
    mailserver.quit()
    
    print("hola ya debio llegar un correo a tu bandeja")
